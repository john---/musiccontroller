/*
    Code is extensively copied from:

    igorantolic: https://github.com/igorantolic/ai-esp32-rotary-encoder
    arduino-timer: https://github.com/contrem/arduino-timer
    nopnop2002: https://github.com/nopnop2002/esp8266-mpd-client
    xn1ch1: https://www.instructables.com/Arduino-Dual-Function-Button-Long-PressShort-Press/
    https://www.mischianti.org/2021/03/26/esp32-practical-power-saving-wake-up-from-light-sleep-via-uart-and-gpio-6/

*/

#include <esp_sleep.h>
#include <driver/uart.h>

#include <TinyPICO.h>

#include "local_info.h"

#if defined(ESP8266)
#include <ESP8266WiFi.h>
#else
#include <WiFi.h>
#endif

#include "AiEsp32RotaryEncoder.h"
#include <arduino-timer.h>

Timer<3> timer;
//auto heartBeatTimer = timer_create_default();
Timer<>::Task hbeat_task;
long heartBeatInterval = 45;  // seconds
Timer<>::Task sleep_task;
long sleepTime = 30;  // minutes
Timer<>::Task turn_off_led_task;
size_t led_fade_time = 1500;  // time to fade
size_t led_fade_steps = 40;   // number of steps to fade in
size_t led_time_per_step = led_fade_time / led_fade_steps;

//int duration = 10; // how long to wait for MPD response
//int duration = 100; // how long to wait for MPD response
int duration = 500;  // how long to wait for MPD response

// Initialise the TinyPICO library
TinyPICO tp = TinyPICO();

// Use WiFiClient class to create TCP connections
WiFiClient client;

int mpc_connect(char const* host, int port) {
  char smsg[40];
  char rmsg[40];

  if (!client.connect(host, port)) {
    Serial.println("connection failed");
    return 0;
  }

  String line;
  client.setTimeout(1000);
  delay(duration);  // TODO: Determine optimal delay
  //Serial.println(client.available());
  line = client.readStringUntil('\n');
  //Serial.print("[");
  //Serial.print(line);
  //Serial.println("]");
  //Serial.println("length()=" + String(line.length()));
  line.toCharArray(rmsg, line.length() + 1);
  //Serial.println("strlen()=" + String(strlen(rmsg)));
  rmsg[line.length()] = 0;
  //Serial.println("rmsg=[" + String(rmsg) + "]");
  if (strncmp(rmsg, "OK", 2) == 0) return 1;
  return 0;
}

int NOMINAL = 1;
int PROBLEM = 2;
int CONNECTING = 3;
int STOP_PLAYING = 4;

size_t led_fade_steps_counter = led_fade_steps;
void turn_on_led(int type) {
  tp.DotStar_SetPower(true);
  if (type == NOMINAL) {
    tp.DotStar_SetPixelColor(255, 140, 0);  // darkorange
    //tp.DotStar_SetPixelColor( 0, 128, 0 );  // green
  } else if (type == PROBLEM) {
    tp.DotStar_SetPixelColor(255, 0, 0);  // red
  } else {
    tp.DotStar_SetPixelColor(0, 0, 255);  // blue
  }

  // TODO: need to add on time before the fade?
  led_fade_steps_counter = led_fade_steps;  // keep like going when another comes in while LED fading
  turn_off_led_task = timer.every(led_time_per_step, turn_off_led);
}

bool turn_off_led(void*) {
  int level = led_fade_steps_counter * (255 / led_fade_steps);
  //Serial.println("fading down: " + String(led_fade_steps_counter) + " level: " + String(level));
  tp.DotStar_SetBrightness(level);
  tp.DotStar_Show();

  if (led_fade_steps_counter == 0) {
    tp.DotStar_SetPower(false);
    led_fade_steps_counter = led_fade_steps;
    return false;
  } else {
    led_fade_steps_counter--;
    return true;
  }
  //return led_fade_steps_counter == 0 ? false : true;
}

int mpc_command(char const* buf) {
  if (strcmp(buf, "ping") != 0) {  // track user action only for determing sleep time
    timer.cancel(hbeat_task);
    hbeat_task = timer.every(heartBeatInterval * 1000, sendHeartBeat);
    timer.cancel(sleep_task);
    sleep_task = timer.in(sleepTime * 1000 * 60, cpuSleep);
  }
  char smsg[40];
  char rmsg[40];
  sprintf(smsg, "%s\n", buf);
  tp.DotStar_SetPower(true);
  tp.DotStar_SetPixelColor(255, 140, 0);  // darkorange
  client.print(smsg);
  Serial.println("smsg=[" + String(buf) + "]");
  String line;
  //client.setTimeout(1000);
  //delay(duration);  // TODO: Determine optimal delay
  //tp.DotStar_SetPower( true );
  //tp.DotStar_SetPixelColor( 0, 255, 240 );  // cyan
  //line = client.readStringUntil('\n');

  while (client.available()) {
    line = client.readStringUntil('\n');
  }

  //tp.DotStar_SetPower( false );
  Serial.print("[");
  Serial.print(line);
  Serial.println("]");
  Serial.println("length()=" + String(line.length()));
  line.toCharArray(rmsg, line.length() + 1);
  Serial.println("strlen()=" + String(strlen(rmsg)));
  rmsg[line.length()] = 0;
  Serial.println("rmsg=[" + String(rmsg) + "]");
  if (strcmp(rmsg, "OK") == 0) {
    if (strcmp(buf, "stop") == 0) {
      turn_on_led(STOP_PLAYING);
    } else {
      turn_on_led(NOMINAL);
    }
    return 1;
  } else {
    turn_on_led(PROBLEM);
    Serial.println("mpc command error: " + String(buf));
  }

  // maybe light the LED here.  Actually, maybe change color based on success of command
  return 0;
}

//void mpc_error(char const* buf) {
//  Serial.print("mpc command error:");
//  Serial.println(buf);
//  // while (1) {}
//}


#define ROTARY_ENCODER_A_PIN 32
#define ROTARY_ENCODER_B_PIN 21
#define ROTARY_ENCODER_BUTTON_PIN 25
#define ROTARY_ENCODER_VCC_PIN -1 /* 27 put -1 of Rotary encoder Vcc is connected directly to 3,3V; else you can use declared output pin for powering rotary encoder */

//depending on your encoder - try 1,2 or 4 to get expected behaviour
//#define ROTARY_ENCODER_STEPS 1
//#define ROTARY_ENCODER_STEPS 2
#define ROTARY_ENCODER_STEPS 4

AiEsp32RotaryEncoder rotaryEncoder = AiEsp32RotaryEncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, ROTARY_ENCODER_BUTTON_PIN, ROTARY_ENCODER_VCC_PIN, ROTARY_ENCODER_STEPS);

bool button_pressed = false;

// This function is noop as I couldn't figure out how to disable it in rotaryEncoder.setup
void rotary_onButtonClick() {
  // Noop
}

void rotary_loop() {
  static int previousValue = 0;
  //dont print anything unless value changed
  if (!rotaryEncoder.encoderChanged()) {
    return;
  }

  int currentValue = rotaryEncoder.readEncoder();
  if (currentValue < previousValue) {
    Serial.println("volume down");
    mpc_command("volume -1");
  } else {
    Serial.println("volume up");
    mpc_command("volume +1");
  }

  previousValue = currentValue;
}

void IRAM_ATTR readEncoderISR() {
  rotaryEncoder.readEncoder_ISR();
}

bool sendHeartBeat(void*) {

  mpc_command("ping");

  return true;
}

bool cpuSleep(void*) {
  Serial.println("about to sleep due to lack of user input");
  delay(100);
  client.stop();
  delay(500);
  WiFi.disconnect(true);
  esp_deep_sleep_start();

  return false;
}

void setup() {
  Serial.begin(115200);

  //tp.DotStar_SetBrightness(64);
  //tp.DotStar_SetBrightness(255);
  //tp.DotStar_Show();

  esp_sleep_enable_ext0_wakeup(GPIO_NUM_25, 1);

  Serial.print("Is battery charging: ");
  Serial.println(tp.IsChargingBattery());

  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Wait for WiFi...");
  turn_on_led(CONNECTING);
  WiFi.begin(ssid, password);

  int cnt = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    cnt++;
    if ((cnt % 60) == 0) Serial.println();
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  while (1) {
    Serial.print("connecting to ");
    Serial.println(host);
    if (mpc_connect(host, port) == 1) {
      Serial.println("connected");
      break;
    }
    delay(10 * 1000);
  }

  //we must initialize rotary encoder
  pinMode(ROTARY_ENCODER_A_PIN, INPUT_PULLDOWN);
  pinMode(ROTARY_ENCODER_B_PIN, INPUT_PULLDOWN);
  rotaryEncoder.begin();
  rotaryEncoder.setup(readEncoderISR);
  rotaryEncoder.disableAcceleration();  //acceleration is now enabled by default - disable if you dont need it

  hbeat_task = timer.every(heartBeatInterval * 1000, sendHeartBeat);
  sleep_task = timer.in(sleepTime * 1000 * 60, cpuSleep);

  // doing play twice as it seems when starting up first time errors out
  mpc_command("play");  // Start if stopped
  mpc_command("play");
}


long buttonTimer = 0;
long longPressTime = 750;

boolean buttonActive = false;
boolean longPressActive = false;

void loop() {

  timer.tick();

  if (!client.connected()) {
    Serial.println("server disconencted");
    delay(10 * 1000);
    ESP.restart();
  }

  rotary_loop();

  if (digitalRead(ROTARY_ENCODER_BUTTON_PIN) == HIGH) {
    if (buttonActive == false) {
      buttonActive = true;
      buttonTimer = millis();
    }
    if ((millis() - buttonTimer > longPressTime) && (longPressActive == false)) {
      // long press
      longPressActive = true;
      mpc_command("stop");
    }
  } else {
    if (buttonActive == true) {
      if (longPressActive == true) {
        longPressActive = false;
      } else {
        // short press
        mpc_command("play");  // Start if stopped
        mpc_command("next");
      }

      buttonActive = false;
    }
  }
}