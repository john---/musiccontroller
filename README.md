Uses an ESP32 based device (UM TinyPICO) to control MPD over 802.11.

Arduino IDE: File|Preferences and add this into Additional Board Manager URLs" (comma seperate list):

https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json

More detail on board installation:

https://docs.espressif.com/projects/arduino-esp32/en/latest/installing.html#installing-using-boards-manager

Tools|Manage Libraries and install:

- TinyPICO_Helper_Library
- Ai_Esp32_Rotary_Encoder
- arduino-timer